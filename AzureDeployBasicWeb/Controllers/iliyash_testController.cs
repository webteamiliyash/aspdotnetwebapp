﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AzureDeployBasicWeb;

namespace AzureDeployBasicWeb.Controllers
{
    public class iliyash_testController : Controller
    {
        private iliyash_testEntities db = new iliyash_testEntities();

        // GET: iliyash_test
        public ActionResult Index()
        {
            return View(db.iliyash_test.ToList());
        }

        // GET: iliyash_test/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            iliyash_test iliyash_test = db.iliyash_test.Find(id);
            if (iliyash_test == null)
            {
                return HttpNotFound();
            }
            return View(iliyash_test);
        }

        // GET: iliyash_test/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: iliyash_test/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name")] iliyash_test iliyash_test)
        {
            if (ModelState.IsValid)
            {
                db.iliyash_test.Add(iliyash_test);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iliyash_test);
        }

        // GET: iliyash_test/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            iliyash_test iliyash_test = db.iliyash_test.Find(id);
            if (iliyash_test == null)
            {
                return HttpNotFound();
            }
            return View(iliyash_test);
        }

        // POST: iliyash_test/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name")] iliyash_test iliyash_test)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iliyash_test).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iliyash_test);
        }

        // GET: iliyash_test/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            iliyash_test iliyash_test = db.iliyash_test.Find(id);
            if (iliyash_test == null)
            {
                return HttpNotFound();
            }
            return View(iliyash_test);
        }

        // POST: iliyash_test/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            iliyash_test iliyash_test = db.iliyash_test.Find(id);
            db.iliyash_test.Remove(iliyash_test);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
